from app import *
from werkzeug import secure_filename
from flask import send_from_directory, Markup
import json

# Load custom 404 page on 404 errors
@app.errorhandler(404)
def notFoundError(error):
    # return("The page you're looking for doesn't seem to exist. <a href='/'>Click me</a> to go back to the homepage.")
    return(render_template("404.html"),404)

@app.route("/")
def root():
    title = "Home"
    sidebar = db.getMOTD()
    pageURLs = [ i["file_name"] for i in pc.getPages() ]
    if "home" in pageURLs:
        return(render_template("generated_pages/home.html", **locals()))
    else:
        return(render_template("home.html", **locals()))

@app.route("/admin", methods=["GET","POST"])
@AdminRequired
def admin():
    if("newuserpass" not in locals()):
        newuserpass=None

    if(request.method == "POST"):
        if(request.form["purp"] == "addUser"):
            print(request.form["newUser"])
            newuserpass=userMng.createUser(request.form["newUser"], role="editor")
        if(request.form["purp"] == "delUser"):
            db.delUser(request.form["username"])

    title = "Admin Panel"
    users = db.getUsers()
    return(render_template("admin.html", **locals()))

@app.route("/account", methods=["GET","POST"])
@LoginRequired
def account():
    title = "Account Settings"
    user = db.getUser(session["username"])
    if(request.method == "POST"):
        old_password = request.form["old_password"]
        password = request.form["new_password"]
        password_check = request.form["new_password_check"]
        if(password == password_check):
            print(user, old_password, password)
            if(userMng.updatePasswd(session["username"], old_password, password)):
                flash("Password changed", "success")
            else:
                flash("Wrong password", "danger")
        else:
            flash("Passwords don't match", "danger")
    return(render_template("account.html", **locals()))

@app.route("/logout", methods=["GET","POST"])
def logout():
    session.pop("loggedin")
    session.pop("username")
    flash("Logout successful", "success")
    return(redirect("/"))

# Login page
@app.route("/login", methods=["GET","POST"])
def login():
    # If user is already logged in, redirect them to the root page
    if("loggedin" in session):
        return(redirect("/"))

    # If the request is sent as POST, run checkUser with form data
    if(request.method == "POST"):
        username = request.form["username"]
        password = request.form["password"]

        # checkUser returns True if correct user and passwd
        print("CHECKING USER")
        print(userMng.checkUser(username, password))
        if userMng.checkUser(username, password):
            print("Login correct")
            session["username"] = username
            session["role"] = db.getUser(username)['role']
            session["loggedin"] = True
            flash("Log in successful", "success")
            return(redirect("/"))
        else:
            print("Login Wrong")
            flash("Wrong username or password", "danger")

    return(render_template("login.html", title="Login"))

@app.route("/mediamanager")
@AdminRequired
def media_manager():
    fc = FileController()
    files = fc.getFiles()
    return(render_template("media.html", files=files, title="Media Manager"))

@app.route("/delmedia", methods=["POST"])
def delmedia():
    filename = request.form["filename"]
    fc = FileController()
    fc.delFile(filename)
    print(filename)
    return(redirect("/mediamanager"))

@app.route("/upload", methods=["POST"])
@LoginRequired
def upload():
    # check if the post request has the file part
    if 'fileToUpload' not in request.files:
        result = json.dumps({"success":False})
    file = request.files['fileToUpload']
    # if user does not select file, browser also
    # submit an empty part without filename
    if file.filename == '':
        return redirect(request.url)
        result = json.dumps({"success":False})
    if file:
        fc = FileController()
        filename = fc.uploadFile(file)
        # return redirect(url_for('uploaded_file', filename=filename))
        if not filename:
            result = json.dumps({"success":False})
        else:
            result = json.dumps({"success":True, "file":url_for('uploaded_file', filename=filename)})
    return(result)

@app.route("/uploads/<filename>")
def uploaded_file(filename):
    return send_from_directory("static/uploaded_file", filename)

@app.route("/editmotd")
@LoginRequired
def editMOTD():
    title = "Edit MOTD"
    sb = db.getMOTD()
    sidebar = sb
    motd = sb
    return(render_template("editmotd.html", **locals()))

@app.route("/newmotd",methods=["POST"])
def newmotd():
    newData = request.form["new_motd"]
    db.addMOTD(newData)
    return(redirect("/editmotd"))

@app.route("/delmotd", methods=["POST"])
def delmotd():
    delID = request.form["motdID"]
    db.delMOTD(delID)
    return(redirect("/editmotd"))

@app.route("/updatemotd", methods=["POST"])
def updatemotd():
    updatedID = request.form["motdID"]
    updatedContent = request.form["updatedMotd"]
    db.cngMOTD(updatedID, updatedContent)
    return(redirect("/editmotd"))

# URLs from editable pages
@app.route("/<pageURL>")
def autoPage(pageURL):
    # Get list of known file_names/urls from PageController
    pageURLs = [ i["file_name"] for i in pc.getPages() ]

    # Return 404 page doesn't exist
    if(pageURL not in pageURLs):
        return(abort(404))

    # Get page information for page with entered URL
    page = [ i for i in pc.getPages() if i["file_name"]==pageURL ][0]
    #content = pc.getPage(page["id"])
    pagePath = pc.getPagePath(page["id"])
    title = page["title"]

    # If the sidebar is enables, set sidebar
    if page["sidebar"]:
        sidebar = db.getMOTD()
    else:
        sidebar = False

    return(render_template(pagePath, **locals()))

@app.route("/editpages",methods=["GET","POST"])
@LoginRequired
def editPages():
    title = "Edit Pages"

    if(request.method == "POST"):
        # Check purpose of request using hidden input "purp"
        if(request.form["purp"] == "editPage"):
            # Load the page editing interface
            pageID = request.form["pageID"]
            pageContent = pc.getPage(pageID)["content"]
            pageTitle = pc.getPage(pageID)["title"]
            sb = pc.getPage(pageID)["sidebar"]
            tb = pc.getPage(pageID)["toolbar"]
            title = "Edit Page"
            return(render_template("editpage.html",**locals()))
        elif(request.form["purp"] == "addPage"):
            title = "Add Page"
            return(render_template("addpage.html", **locals()))
        elif(request.form["purp"] == "submitEdit"):
            # Pull form contents and pass them to the page controller
            pageID = request.form["pageID"]
            pageContent = request.form["content"]
            pageTitle = request.form["title"]
            sb = True if "sidebar" in request.form.keys() else False
            tb = True if "toolbar" in request.form.keys() else False
            # The addPage method doubles as page editing if its given a pageID
            pc.addPage(
                pageTitle,
                pageContent,
                # app.url_map.iter_rules() gives all app URL endpoints; cleaned up by splitting at /
                exclusions=[str(rule).split("/")[1] for rule in app.url_map.iter_rules()],
                sb = sb,
                tb = tb,
                pageID = pageID
            )
            flash("Page Updated", "success")
        elif(request.form["purp"] == "submitNew"):
            # Pull form contents and pass them to the page controller
            if "sidebar" in request.form.keys():
                print(request.form["sidebar"])
            pageContent = request.form["content"]
            pageTitle = request.form["title"]
            url = request.form["url"] if request.form["url"] else pageTitle
            sb = True if "sidebar" in request.form.keys() else False
            tb = True if "toolbar" in request.form.keys() else False
            pc.addPage(
                pageTitle,
                pageContent,
                url=url,
                # app.url_map.iter_rules() gives all app URL endpoints; cleaned up by splitting at /
                exclusions=[ str(rule).split("/")[1] for rule in app.url_map.iter_rules() ],
                sb = sb,
                tb = tb
            )
        else:
            return(redirect("/editpages"))
    pages = pc.getPages()
    return(render_template("editpages.html",**locals()))

@app.route("/delpage", methods=["POST"])
def delpage():
    delID = request.form["pageID"]
    pc.delPage(delID)
    return(redirect("/editpages"))
