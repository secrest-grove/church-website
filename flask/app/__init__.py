import os
from flask import Flask, abort, render_template, redirect, request, url_for, session, flash
from functools import wraps
from app.DatabaseController import DatabaseController
from app.UserController import UserController
from app.PageController import PageController
from app.FileController import FileController
import markdown

app = Flask(__name__)

@app.after_request
def add_header(response):
    response.cache_control.no_store = True
    response.cache_control.no_cache = True
    return response

# Set app secret key used for session cookies
app.secret_key = os.urandom(100)

app.jinja_env.globals["markdown"] = markdown
app.jinja_env.globals["session"] = session

# Start the database and page controllers
db = DatabaseController(os.getenv("DB_URI"))
pc = PageController(db)
userMng = UserController(db)

def getToolbar():
    items = [ (page["title"], page["file_name"]) for page in db.getToolbarPages() ]
    return(items)

app.jinja_env.globals.update(getToolbar=getToolbar)

# Decorator funcitons
def LoginRequired(func):
    @wraps(func)
    def wrapper(*args,**kwargs):
        if("loggedin" not in session or session["loggedin"] != True):
            return(redirect("/login"))
        elif(session["loggedin"] == True):
            return(func(*args,**kwargs))
    return(wrapper)

def AdminRequired(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        if("role" not in session or session["role"] != "admin"):
            return(redirect("/login"))
        elif(session["role"] == "admin"):
            return(func(*args, **kwargs))
    return(wrapper)

from app import views
