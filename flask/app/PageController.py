import os
from werkzeug import secure_filename
from flask import Markup

class PageController:
    def __init__(self, db):
        # Template settings
        self.prefix = "{% extends 'base.html' %}\n{% block body %}\n\n"
        self.suffix = "\n\n{% endblock %}"
        self.folder = "app/templates/generated_pages/"

        # Set up db
        self.db = db

        # Create template folder if it doesn't exist
        if not os.path.isdir(self.folder):
            os.mkdir(self.folder)

    def addPage(self,title,content,url=None,sb=True,tb=False,exclusions=[],pageID=None):

        # If being used in edit mode
        if pageID:
            fileName = self.db.getPage(pageID)["file_name"]

            # Remove old files
            os.remove(os.path.join(self.folder, fileName))
            os.remove(os.path.join(self.folder, fileName + ".html"))
        else:
            fileNameOrg = secure_filename(url).lower()
            fileName = fileNameOrg
            exclusionList = [page["file_name"] for page in self.db.getPages()] + exclusions

            # Auto increment file name if already used
            i = 1
            print(exclusionList)
            while(fileName in exclusionList):
                print("File name already exists")
                print(fileName)
                print(exclusionList)
                fileName = fileNameOrg + str(i)
                i+=1

        # Write content file and html template
        with open(self.folder + fileName, "w") as file:
            file.write(content)
            file.truncate()
        with open(self.folder + fileName + ".html", "w") as file:
            file.write(Markup(self.prefix + content + self.suffix))
            file.truncate()

        # Edit mode switch
        if pageID:
            self.db.editPage(pageID, title, sb, tb)
        else:
            self.db.addPage(title, fileName, sb, tb)

        return()

    def delPage(self,pageID):
        file_name = self.db.getPage(pageID)["file_name"]
        os.remove(os.path.join(self.folder, file_name))
        os.remove(os.path.join(self.folder, file_name + ".html"))
        self.db.delPage(pageID)
        return()

    def getPage(self,pageID,template=True):
        page = self.db.getPage(pageID)
        if template:
            # Return the content
            with open(self.folder + page["file_name"], "r") as file:
                result = file.read()
        else:
            # Return full html template
            with open(self.folder + page["file_name"] + ".html", "r") as file:
                result = file.read()
        return({
            "title":page["title"],
            "content":result,
            "sidebar":page["sidebar"],
            "toolbar":page["toolbar"]
        })

    def getPagePath(self,pageID):
        file_name = self.db.getPage(pageID)["file_name"]
        return "generated_pages/" + file_name + ".html"

    def getPages(self):
        # Return list of pages
        return(self.db.getPages())
