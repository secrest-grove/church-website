from app.DatabaseController import DatabaseController
import string
import random
import hashlib, uuid

class UserController():
    def __init__(self, db):
        self.db = db

    def createUser(self, user, passwd=None, role=None):
        salt = uuid.uuid4().hex

        tempPass = ""
        if(not passwd):
            tempPass = ''.join([ random.choice(string.ascii_letters) for i in range(10)])
            passwd = tempPass

        hashed_passwd = hashlib.sha256((passwd + salt).encode()).hexdigest()

        try:
            self.db.addUser(user, hashed_passwd, salt, role)
        except:
            return("Error")

        return(tempPass)

    def checkUser(self, user, passwd):
        userInfo = self.db.getUser(user)

        # Return false if user isn't found
        if not userInfo:
            return(False)

        salt = userInfo["salt"]
        hashed_passwd = hashlib.sha256((passwd + salt).encode()).hexdigest()
        return(bool(hashed_passwd == userInfo["passwd"]))

    def updatePasswd(self, user, passwd, newPasswd):
        if(self.checkUser(user, passwd)):
            salt = uuid.uuid4().hex
            hashed_passwd = hashlib.sha256((newPasswd + salt).encode()).hexdigest()
            self.db.updateUser(user, passwd=hashed_passwd, salt=salt)
            return(True)
        else:
            return(False)
