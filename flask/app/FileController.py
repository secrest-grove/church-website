import os
from werkzeug import secure_filename

class FileController():
    def __init__(self):
        self.upload_folder = "app/static/uploaded_file"
        self.allowed_extensions = set(['png', 'jpg', 'jpeg', 'gif'])

        if not os.path.isdir(self.upload_folder):
            os.mkdir(self.upload_folder)

    def allowedFile(self, filename):
        return '.' in filename and \
            filename.rsplit('.', 1)[1].lower() in self.allowed_extensions

    def uploadFile(self, file):

        if not self.allowedFile(file.filename):
            return(False)

        file_list = []
        for dir in os.walk("app/static/uploaded_file"):
            print(dir[2])
            file_list = dir[2]

        i = 1
        new_file_name = secure_filename(file.filename)
        while new_file_name in file_list:
            split_file_name = secure_filename(file.filename).split(".")
            new_file_name = ".".join(split_file_name[0:-1]) + str(i) + "." + split_file_name[-1]
            i += 1

        file.save(os.path.join(self.upload_folder, new_file_name))

        return(new_file_name)

    def getFiles(self):
        files = os.scandir("app/static/uploaded_file")

        return(files)

    def delFile(self, filename):
        os.remove(os.path.join(self.upload_folder, filename))
        return()
