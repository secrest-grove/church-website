import time
import threading
from functools import wraps
from sqlalchemy import Column, Integer, String, Float, Boolean
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

Base = declarative_base()


# Declare table structures
class MOTD(Base):
    __tablename__ = 'motd'
    id = Column(Integer, primary_key=True)
    msg = Column(String(256))
    time = Column(Float)


class User(Base):
    __tablename__ = 'user'
    id = Column(Integer, primary_key=True)
    username = Column(String(32), unique=True)
    salt = Column(String(256))
    passwd = Column(String(256))
    role = Column(String(32))


class PageRelation(Base):
    __tablename__ = "page"
    id = Column(Integer, primary_key=True)
    title = Column(String(32))
    file_name = Column(String(32), unique=True)
    sidebar = Column(Boolean())
    toolbar = Column(Boolean())


class DatabaseController():
    def __init__(self, engineURI):
        # Bind engine and create tables
        self.engine = create_engine(engineURI, pool_recycle=3600)
        Base.metadata.create_all(bind=self.engine)

    # wrappter function to handle opening and closeing of db connection
    def AccessDatabase(func):
        @wraps(func)
        def wrapper(self, *args, **kwargs):
            Session = sessionmaker(bind=self.engine)
            self.session = Session()
            self.dbLock = threading.Lock()
            result = func(self, *args, **kwargs)
            self.session.commit()
            self.session.close()
            return result
        return wrapper

    @AccessDatabase
    def addMOTD(self, msg):
        newMOTD = MOTD()
        newMOTD.msg = msg
        newMOTD.time = time.time()
        self.session.add(newMOTD)

    @AccessDatabase
    def getMOTD(self):
        # x.__dict__ returs objects attributes as a dict
        # return list of motd entries
        motd = [x.__dict__ for x in self.session.query(MOTD).all()]
        return(list(reversed(motd)))

    @AccessDatabase
    def delMOTD(self, delID):
        model = self.session.query(MOTD).filter(MOTD.id == delID).first()
        self.session.delete(model)

    @AccessDatabase
    def cngMOTD(self, cngID, msg):
        model = self.session.query(MOTD).filter(MOTD.id == cngID).first()
        model.msg = msg

    @AccessDatabase
    def addUser(self, username, passwd, salt, role):
        newUser = User()
        newUser.username = username
        newUser.passwd = passwd
        newUser.salt = salt
        newUser.role = role
        self.session.add(newUser)

    @AccessDatabase
    def getUser(self, username):
        # .first() will return None if filter found none
        user = self.session.query(User).filter(
                User.username == username).first()
        if not user:
            result = None
        else:
            result = user.__dict__
        return(result)

    @AccessDatabase
    def updateUser(self, username, passwd=None, role=None, salt=None):
        print(username)
        user = self.session.query(User).filter(
                User.username == username).first()
        if(passwd):
            user.passwd = passwd
        if(role):
            user.role = role
        if(salt):
            user.salt = salt

    @AccessDatabase
    def delUser(self, username):
        user = self.session.query(User).filter(
                User.username == username).first()
        self.session.delete(user)

    @AccessDatabase
    def getUsers(self):
        # Return list over users and attributes
        users = self.session.query(User).all()
        users = [i.__dict__ for i in users]
        return(users)

    @AccessDatabase
    def addPage(self, title, file_name, sb, tb):
        page = PageRelation()
        page.title = title
        page.file_name = file_name
        page.sidebar = sb
        page.toolbar = tb
        self.session.add(page)

    @AccessDatabase
    def getPage(self, pageID):
        page = self.session.query(PageRelation).filter(
                PageRelation.id == pageID).first()
        return(page.__dict__)

    @AccessDatabase
    def getPages(self):
        pages = self.session.query(PageRelation).all()
        pages = [x.__dict__ for x in pages]
        return(pages)

    @AccessDatabase
    def getToolbarPages(self):
        pages = self.session.query(PageRelation).filter(
                PageRelation.toolbar).all()
        pages = [x.__dict__ for x in pages]
        return(pages)

    @AccessDatabase
    def editPage(self, pageID, newTitle, sb, tb):
        page = self.session.query(PageRelation).filter(
                PageRelation.id == pageID).first()
        page.title = newTitle
        page.sidebar = sb
        page.toolbar = tb

    @AccessDatabase
    def delPage(self, pageID):
        model = self.session.query(PageRelation).filter(
                PageRelation.id == pageID).first()
        self.session.delete(model)
