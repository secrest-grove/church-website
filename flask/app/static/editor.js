$(document).ready(function(){
    $("#editor").trumbowyg({
        autogrowOnEnter: true,
        imageWidthModalEdit: true,
        btnsDef: {
            // Create a new dropdown
            image: {
                dropdown: ['insertImage', 'upload'],
                ico: 'insertImage'
            }
        },
        // Redefine the button pane
        btns: [
            ['viewHTML'],
            ['formatting'],
            ['strong', 'em', 'del'],
            ['superscript', 'subscript'],
            ['link'],
            ['image'], // Our fresh created dropdown
            ['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
            ['unorderedList', 'orderedList'],
            ['horizontalRule'],
            ['removeformat'],
            ['fullscreen']
        ],
        plugins: {
            upload: {
                serverPath: "upload",
                imageWidthModalEdit: true
            }
        }
    });

    $("#editor").trumbowyg("html", $("#content").val());

    $("#editor").on("tbwchange", function(){
        console.log("Editor Changed");
        console.log($(this).trumbowyg("html"));
        $("#content").val($(this).trumbowyg("html"));
    });

});
