# Church website
[![pipeline status](https://gitlab.com/ASCIIbreakdown/church-website/badges/development/pipeline.svg)](https://gitlab.com/ASCIIbreakdown/church-website/commits/development)

## Custom homepage
When loading the root page, Flask will render a prebuilt "under construction" template. If a page is created with the custom url set to `home`, Flask will render that at the root url as well as the custom url.  
Thus, your "homepage" can be accessed from two views: `/` and `/home`

## Cloning the repository
```
git clone git@gitlab.com:ASCIIbreakdown/church-website.git
cd church-website/
```

## Running flask dev server

Python's `virtualenv` is suggested.

```
cd flask/
virtualenv venv
source venv/bin/activate
pip install -r requirements.txt
python run.py
```

# Docker setup
This project uses 3 different docker images:
- flask
- nginx
- mysql

```
+-----------------------------------------+
|                                         |
|         Docker-compose network          |
|                                         |
|              +-----------+              |
|              |           |              |
|              |   flask   |              |
|              |           |              |
|              +-----------+              |
|                  |   |                  |
|        port 8080 |   |  port 3306       |
|                  |   |                  |
|  +---------------+   +---------------+  |
|  |           |           |           |  |
|  |   nginx   |           |   mysql   |  |
|  |           |           |           |  |
|  +-----------------+     +-----------+  |
|                    |                    |
|                    | port 80            |
|                    |                    |
+-----------------------------------------+
                     |
             port 80 |        +-----------+
                     |        |           |
                     +--------+  The Web  |
                              |           |
                              +-----------+
```

## Docker install
```
docker-compose -f docker-compose.dev.yml build
docker-compose -f docker-compose.dev.yml up
```

## MySQL server setup
The MySQL server needs to be setup manually.
Login to the MySQL server
```
docker-compose -f docker-compose.dev.yml exec mysql mysql
```
SQLAlchemy requires a special user setting for login to work.
Create user and give it access to the main database
```
CREATE USER 'flask'@'%' IDENTIFIED WITH mysql_native_password BY "password";
CREATE DATABASE main;
GRANT ALL ON main.* TO 'flask'@'%';
```
The flask container will need to be restarted after the flask user is added:
```
docker-compose -f docker-compose.dev.yml restart flask
```

# Flask setup
A new user will need to be created for the website.
## Set up user
Run `python` in the flask container
```
docker-compose -f docker-compose.dev.yml exec flask python
```
Create new user:
```
from app import *
userMng.createUser("<username>","<password>","<role>")
```
